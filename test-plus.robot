*** Settings ***
Library           RequestsLibrary


*** Test Cases ***
Test Calculate Numbers 5 and 6

    ${resp}=     GET    http://127.0.0.1:5000/plus/5/6

    # Verify the status code is 200 (OK)
    Should be equal               ${resp.status_code}    ${200}

    # Verify the response of plus operation
    Should Be Equal As Numbers    ${resp.text}           ${11}


Test Calculate Numbers 1.1 and 1.1

    ${resp}=     GET    http://127.0.0.1:5000/plus/1.1/1.1

    # Verify the status code is 200 (OK)
    Should be equal               ${resp.status_code}    ${200}

    # Verify the response of plus operation
    Should Be Equal As Numbers    ${resp.text}           ${2.2}


Test Calculate Numbers 5 and -6

    ${resp}=     GET    http://127.0.0.1:5000/plus/5/-6

    # Verify the status code is 200 (OK)
    Should be equal               ${resp.status_code}    ${200}

    # Verify the response of plus operation
    Should be equal as Numbers    ${resp.text}           ${-1}
